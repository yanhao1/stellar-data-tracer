from trades_trace import weekly_trade_data
import time
from datetime import datetime, timedelta

init = 15383682

initial = datetime(2018,1,1)

total_days = 7

ledgers_daily = 86400 / 5 # 86400s per day, 5s per ledger

for i in range(total_days):
    start = init + i*ledgers_daily
    end = init + (i+1)*ledgers_daily
    outputFile = f'trades_for_{str(initial + timedelta(i))[:10]}.csv'
    print(f'start to collect {str(initial + timedelta(i))[:10]} data...')
    print(datetime.utcnow())
    try:
        weekly_trade_data(outputFile, int(start), int(end))
        print(f'succeed to collect {str(initial + timedelta(i))[:10]} data')
        print(datetime.utcnow())
    except Exception as err:
        print(f'Error: {err}')
    time.sleep(120)  # sleep 2 min before next move